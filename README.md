DEPRECATED: This is the old semi-broken Python version, see [TV-WoL-RS](https://github.com/teozkr/tv-wol-rs) for the somewhat maintained Rust rewrite.

# TV-WoL

Wake on LAN for HDMI-CEC. Listens on a random TCP port, turns on the TV when there is at least one connection, turns it off when there are no connections.
